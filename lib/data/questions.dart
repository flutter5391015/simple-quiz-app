import 'package:quiz_app/models/quiz_question.dart';

final questions = [
  QuizQuestion(
    id: 1,
    question: 'What are the main building blocks of flutter ui? ',
    choices: [
      'Widgets',
      'Components',
      'Blocks',
      'Functions',
    ],
    answer: 'Widgets',
  ),
  QuizQuestion(
    id: 2,
    question: 'How are Flutter UIs built?',
    choices: [
      'By combining widgets in code',
      'By combining widgets in a visual editor',
      'By defining widgets in config files',
      'By using XCode for iOS and Android Studio for Android',
    ],
    answer: 'By combining widgets in code',
  ),
  QuizQuestion(
    id: 3,
    question: 'What\'s the purpose of a StatefulWidget?',
    choices: [
      'Update UI as data changes',
      'Update data as UI changes',
      'Ignore data changes',
      'Render UI that does not depend on data',
    ],
    answer: 'Update UI as data changes',
  ),
  QuizQuestion(
    id: 4,
    question:
        'Which widget should you try to use more often: StatelessWidget or StatefulWidget?',
    choices: [
      'StatelessWidget',
      'StatefulWidget',
      'Both are equally good',
      'None of the above',
    ],
    answer: 'StatelessWidget',
  ),
  QuizQuestion(
    id: 5,
    question: 'What happens if you change data in a StatelessWidget?',
    choices: [
      'The UI is not updated',
      'The UI is updated',
      'The closest StatefulWidget is updated',
      'Any nested StatefulWidgets are updated',
    ],
    answer: 'The UI is not updated',
  ),
  QuizQuestion(
    id: 6,
    question: 'How should you update data inside of StatefulWidgets?',
    choices: [
      'By calling setState()',
      'By calling updateData()',
      'By calling updateUI()',
      'By calling updateState()',
    ],
    answer: 'By calling setState()',
  ),
];
