import 'package:flutter/material.dart';
import 'package:quiz_app/pages/question_screen.dart';
import 'package:quiz_app/pages/start_screen.dart';
import 'package:quiz_app/models/quiz_answer.dart';
import 'package:quiz_app/data/questions.dart';
import 'package:quiz_app/pages/result_screen.dart';

class QuizScreen extends StatefulWidget {
  const QuizScreen({super.key});

  @override
  State<QuizScreen> createState() => _QuizScreenState();
}

class _QuizScreenState extends State<QuizScreen> {
  List<QuizAnswer> selectedAnswer = [];
  String activeScreen = 'start-screen';

  void switchScreen() {
    setState(() {
      activeScreen = 'question-screen';
    });
  }

  void onReset() {
    setState(() {
      selectedAnswer = [];
      activeScreen = 'start-screen';
    });
  }

  void chooseAnswer(QuizAnswer answer) {
    if (selectedAnswer.length == questions.length) {
      setState(() {
        activeScreen = 'result-screen';
      });
    } else {
      selectedAnswer.add(answer);
    }
  }

  @override
  Widget build(BuildContext context) {
    final Widget? screenWidget;
    if (activeScreen == 'start-screen') {
      screenWidget = StartScreen(onPressed: switchScreen);
    } else if (activeScreen == 'question-screen') {
      screenWidget = QuestionScreen(
        onSelectAnswer: chooseAnswer,
      );
    } else {
      screenWidget = ResultScreen(
        onSelectedAnswer: selectedAnswer,
        onReset: onReset,
      );
    }

    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [Colors.deepPurple, Colors.indigo],
          ),
        ),
        child: screenWidget,
      ),
    );
  }
}
