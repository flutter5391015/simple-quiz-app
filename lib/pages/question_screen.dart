import 'package:flutter/material.dart';
import 'package:quiz_app/models/quiz_answer.dart';
import 'package:quiz_app/models/quiz_question.dart';
import 'package:quiz_app/widgets/qz_choices.dart';
import 'package:quiz_app/data/questions.dart';

class QuestionScreen extends StatefulWidget {
  final void Function(QuizAnswer answer) onSelectAnswer;
  const QuestionScreen({super.key, required this.onSelectAnswer});

  @override
  State<QuestionScreen> createState() => _QuestionScreenState();
}

class _QuestionScreenState extends State<QuestionScreen> {
  int number = 0;
  QuizQuestion currentQuestions = questions[0];

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(mainAxisSize: MainAxisSize.min, children: [
        QzChoices(
            question: currentQuestions.question,
            choices: currentQuestions.getShuffledAnswer(),
            onAnswerClicked: (value) {
              widget.onSelectAnswer(QuizAnswer(
                  question: currentQuestions,
                  selectedAnswer: value,
                  isCorrectAnswer: currentQuestions.answer.toLowerCase() ==
                      value.toLowerCase()));

              setState(() {
                number += 1;
                if (number <= questions.length - 1) {
                  currentQuestions = questions[number];
                }
              });
            })
      ]),
    );
  }
}
