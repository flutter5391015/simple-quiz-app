import 'package:flutter/material.dart';
import 'package:quiz_app/widgets/qz_text.dart';

class StartScreen extends StatelessWidget {
  final VoidCallback onPressed;
  const StartScreen({super.key, required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(mainAxisSize: MainAxisSize.min, children: [
        Image.asset(
          'assets/images/quiz-logo.png',
          width: 200,
          color: const Color.fromRGBO(255, 255, 255, 0.795),
        ),
        const SizedBox(
          height: 20,
        ),
        const QzText(
          textValue: 'Learn Flutter the fun way!',
          fontSize: 20,
        ),
        const SizedBox(
          height: 20,
        ),
        OutlinedButton.icon(
          onPressed: onPressed,
          icon: const Icon(
            Icons.arrow_right_alt,
          ),
          style: OutlinedButton.styleFrom(foregroundColor: Colors.white),
          label: const QzText(textValue: 'Start Quiz', fontSize: 15),
        )
      ]),
    );
  }
}
