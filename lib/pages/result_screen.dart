import 'package:flutter/material.dart';
import 'package:quiz_app/models/quiz_answer.dart';
import 'package:quiz_app/widgets/qz_question_summary.dart';
import 'package:quiz_app/widgets/qz_text.dart';
import 'package:quiz_app/data/questions.dart';

class ResultScreen extends StatelessWidget {
  final List<QuizAnswer> onSelectedAnswer;
  final VoidCallback onReset;
  const ResultScreen(
      {super.key, required this.onSelectedAnswer, required this.onReset});

  @override
  Widget build(BuildContext context) {
    final totalCorrect =
        onSelectedAnswer.where((element) => element.isCorrectAnswer).length;
    final totalQuestion = questions.length;

    return Center(
      child: Container(
        padding: const EdgeInsets.all(14),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            QzText(
              textValue:
                  'You answered $totalCorrect of $totalQuestion question correctly',
              fontSize: 20,
              textAlign: TextAlign.center,
            ),
            const SizedBox(
              height: 15,
            ),
            QzQuestionSummary(data: onSelectedAnswer),
            const SizedBox(
              height: 30,
            ),
            TextButton.icon(
              icon: const Icon(
                Icons.repeat,
                color: Colors.white,
              ),
              onPressed: onReset,
              style: TextButton.styleFrom(
                foregroundColor: Colors.transparent,
              ),
              label: const QzText(
                textValue: 'Reset Quiz .. ',
                fontSize: 14,
                textAlign: TextAlign.center,
              ),
            )
          ],
        ),
      ),
    );
  }
}
