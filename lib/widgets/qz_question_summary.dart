import 'package:flutter/material.dart';
import 'package:quiz_app/models/quiz_answer.dart';
import 'package:quiz_app/widgets/qz_summary_identifier.dart';
import 'package:quiz_app/widgets/qz_text.dart';

class QzQuestionSummary extends StatelessWidget {
  final List<QuizAnswer> data;
  const QzQuestionSummary({super.key, required this.data});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 400,
      child: SingleChildScrollView(
        child: Column(
          children: data.map((e) {
            return Row(
              children: [
                QzSummaryIdentifier(
                    noIdentifier: e.question.id.toString(),
                    isCorrect: e.isCorrectAnswer),
                const SizedBox(
                  width: 20,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      QzText(
                        textValue: e.question.question,
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      QzText(
                        textValue: e.selectedAnswer,
                        fontSize: 10,
                        fontColor: e.isCorrectAnswer
                            ? const Color.fromARGB(248, 44, 167, 3)
                            : const Color.fromARGB(255, 255, 68, 55),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      QzText(
                        textValue: e.question.answer,
                        fontSize: 10,
                        fontColor: Color.fromARGB(255, 180, 155, 155),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                    ],
                  ),
                ),
              ],
            );
          }).toList(),
        ),
      ),
    );
  }
}
