import 'package:flutter/material.dart';
import 'package:quiz_app/widgets/qz_text.dart';

class QzSummaryIdentifier extends StatelessWidget {
  final String noIdentifier;
  final bool isCorrect;
  const QzSummaryIdentifier(
      {super.key, required this.noIdentifier, required this.isCorrect});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 30,
      width: 30,
      alignment: Alignment.center,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(100),
          color: isCorrect
              ? const Color.fromARGB(248, 44, 167, 3)
              : const Color.fromARGB(255, 255, 68, 55)),
      child: QzText(
        textValue: noIdentifier,
        fontSize: 14,
        textAlign: TextAlign.center,
        fontWeight: FontWeight.bold,
      ),
    );
  }
}
