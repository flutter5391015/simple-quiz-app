import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class QzText extends StatelessWidget {
  final String textValue;
  final double fontSize;
  final TextAlign? textAlign;
  final FontWeight? fontWeight;
  final Color? fontColor;

  const QzText(
      {super.key,
      required this.textValue,
      required this.fontSize,
      this.textAlign,
      this.fontWeight,
      this.fontColor});

  @override
  Widget build(BuildContext context) {
    return Text(
      textValue,
      textAlign: textAlign ?? TextAlign.left,
      style: GoogleFonts.plusJakartaSans(
          color: fontColor ?? Colors.white,
          fontSize: fontSize,
          fontWeight: fontWeight ?? FontWeight.normal),
    );
  }
}
