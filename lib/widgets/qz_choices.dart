import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:quiz_app/widgets/qz_text.dart';

class QzChoices extends StatelessWidget {
  final String question;
  final List<String> choices;
  final ValueChanged<String> onAnswerClicked;

  const QzChoices(
      {required this.question,
      required this.choices,
      required this.onAnswerClicked,
      super.key});

  @override
  Widget build(BuildContext context) {
    List<Widget> answerButtons = [];

    for (String choice in choices) {
      answerButtons.add(Container(
        margin: const EdgeInsets.only(bottom: 10),
        child: ElevatedButton(
          onPressed: () {
            onAnswerClicked(choice);
          },
          style: ElevatedButton.styleFrom(
            fixedSize: const Size.fromWidth(300),
            backgroundColor: const Color.fromARGB(255, 43, 2, 85),
            foregroundColor: Colors.white,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(40),
            ),
            padding:
                const EdgeInsets.only(left: 50, right: 50, top: 10, bottom: 10),
          ),
          child: QzText(
            textValue: choice,
            fontSize: 16,
            textAlign: TextAlign.center,
          ),
        ),
      ));
    }

    return Container(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          QzText(
            textValue: question,
            fontSize: 20,
            textAlign: TextAlign.center,
          ),
          const SizedBox(
            height: 30,
          ),
          ...answerButtons
        ],
      ),
    );
  }
}
