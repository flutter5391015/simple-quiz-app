import 'package:quiz_app/models/quiz_question.dart';

class QuizAnswer {
  final QuizQuestion question;
  final String selectedAnswer;
  final bool isCorrectAnswer;

  QuizAnswer(
      {required this.question,
      required this.selectedAnswer,
      required this.isCorrectAnswer});

  @override
  String toString() {
    return 'QuizAnswer(question: $question, selectedAnswer: $selectedAnswer)';
  }
}
