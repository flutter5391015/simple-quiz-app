class QuizQuestion {
  final int id;
  final String question;
  final List<String> choices;
  final String answer;

  QuizQuestion(
      {required this.id,
      required this.question,
      required this.choices,
      required this.answer});

  List<String> getShuffledAnswer() {
    final shuffledList = List.of(choices);
    shuffledList.shuffle();
    return shuffledList;
  }
}
